package rc

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/http/httputil"
)

var ErrResponseFailed = errors.New("response failed with")

// this is package to wrap a rest call.
type RestCall struct {
	name       string
	backendURL string
	client     *http.Client
}

func New(name, backendURL string) RestCall {
	return RestCall{
		name:       name,
		backendURL: backendURL,
		client:     http.DefaultClient,
	}
}

func (rc *RestCall) Request(ctx context.Context, method, path string, data, value any) error {
	buf := new(bytes.Buffer)
	if data != nil {
		if err := json.NewEncoder(buf).Encode(data); err != nil {
			return fmt.Errorf("encoding failed: %w", err)
		}
	}

	req, err := http.NewRequestWithContext(ctx, method, rc.backendURL+path, buf)
	if err != nil {
		return fmt.Errorf("request failed: %w", err)
	}

	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	req.Header.Set("Accept", "*/*")
	req.Header.Set("Cache-Control", "no-cache")

	resp, err := rc.client.Do(req)
	if err != nil {
		return fmt.Errorf("request failed: %w", err)
	}

	defer resp.Body.Close()

	if resp.StatusCode >= http.StatusBadRequest {
		body, _ := httputil.DumpResponse(resp, true)

		return fmt.Errorf("%w: %s", ErrResponseFailed, string(body))
	}

	if value != nil {
		if err := json.NewDecoder(resp.Body).Decode(value); err != nil {
			return fmt.Errorf("decoding failed: %w", err)
		}
	}

	return nil
}
