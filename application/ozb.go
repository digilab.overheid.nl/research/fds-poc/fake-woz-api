package application

import (
	"encoding/base64"
	"fmt"
	"math"
	"net/http"
	"os"
	"sort"
	"strings"
	"time"

	"github.com/go-pdf/fpdf"
	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fake-woz-api/application/model"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fake-woz-api/application/storage/adapter"
	queries "gitlab.com/digilab.overheid.nl/research/fds-poc/fake-woz-api/application/storage/queries/generated"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fake-woz-api/helpers"
)

// simpleAddress is used to pass formatted addresses to the frontend.
type simpleAddress struct {
	ID    uuid.UUID `json:"id"`
	Label string    `json:"label"`
}

const (
	taxTariff     = 0.079
	sewageCharges = 237.0
	wasteCharges  = 366.25
)

func (app *Application) OZBIndex(c *fiber.Ctx) error {
	assessments, err := app.db.Queries.AssessmentList(c.Context())
	if err != nil {
		return fmt.Errorf("assessmentlist retrieval failed: %w", err)
	}

	// Since the API has no pagination, we paginate the data here.
	// IMPROVE: add `limit` and `offset` (in combination with `hasMoreData` or so in the results) to the backend?
	pagination, lowerBound, upperBound := getPagination(len(assessments), c.Query("page"))
	if pagination.NumPages > 1 {
		assessments = assessments[lowerBound:upperBound]
	}

	// Get a possible success message from the session
	successMessage, err := getAndDeleteFromSession(c, "successMessage")
	if err != nil {
		return err
	}

	return c.Render("ozb-index", fiber.Map{
		"successMessage": successMessage,
		"assessments":    assessments,
		"pagination":     pagination,
	})
}

func (app *Application) OZBNew(c *fiber.Ctx) error {
	addresses, err := app.addressesList(c)
	if err != nil {
		return err
	}

	if string(c.Request().Header.Peek("Hx-Request")) == "true" {
		return c.Render("partials/ozb-new-address-list", fiber.Map{
			"addresses": addresses,
		}, "")
	}

	return c.Render("ozb-new", fiber.Map{
		"addresses": addresses,
	})
}

func (app *Application) OZBNewDetails(c *fiber.Ctx) error {
	// // Get the addressID ID from the query string
	addressID, err := uuid.Parse(c.Query("address"))
	if err != nil {
		return fmt.Errorf("address uuid parse failed: %w", err)
	}

	var address model.Address
	if err := app.frag.Request(c.Context(), http.MethodGet, fmt.Sprintf("/addresses/%s", addressID), nil, &address); err != nil {
		return fmt.Errorf("address request failed: %w", err)
	}

	// If the building ID is specified, return the WOZ-waarde of the specified building
	if buildingID := c.Query("building"); buildingID != "" {
		var wozAmount uint64 // Amount in EUR

		// Get the building with the specified ID
		for idx := range address.Buildings {
			if address.Buildings[idx].ID.String() == buildingID {
				wozAmount = calculateWOZInCents(&address.Buildings[idx])

				break
			}
		}

		// Return the WOZ amount in a partial
		return c.Render("partials/ozb-new-details-woz", fiber.Map{
			"amount": wozAmount,
		}, "")
	}

	var people []model.Person
	if err := app.brp.Request(c.Context(), http.MethodGet, fmt.Sprintf("/addresses/%s/people", addressID), nil, &people); err != nil {
		return fmt.Errorf("people based on address request failed: %w", err)
	}

	sort.Slice(people, func(i, j int) bool {
		return people[i].BornAt.Before(*people[j].BornAt)
	})

	// Return the buildings and people in a partial
	return c.Render("partials/ozb-new-details", fiber.Map{
		"addressID": addressID,
		"buildings": address.Buildings,
		"people":    people,
	}, "")
}

func (app *Application) OZBCreate(c *fiber.Ctx) error {
	// Use a custom struct for the request body
	assessment := new(struct {
		AddressID  helpers.NullUUID `form:"address"`
		BuildingID helpers.NullUUID `form:"building"`
		PersonID   helpers.NullUUID `form:"person"`
	})
	if err := c.BodyParser(assessment); err != nil {
		return fmt.Errorf("unmarshal failed: %w", err)
	}

	// Validate the input
	var errs []string // Used to store validation errors

	if !assessment.AddressID.Valid {
		errs = append(errs, "Het adres is vereist.")
	}

	if !assessment.BuildingID.Valid {
		errs = append(errs, "Het gebouw is vereist.")
	}

	if !assessment.PersonID.Valid {
		errs = append(errs, "De geadresseerde is vereist.")
	}

	// In case there are validation errors, return them
	if len(errs) != 0 {
		return c.Render("partials/form-errors", errs, "")
	}

	// Else: request the specified entities from the backend
	address := new(model.Address)
	building := new(model.Building)
	person := new(model.Person)

	addressURL := fmt.Sprintf("/addresses/%s", assessment.AddressID.UUID)
	if err := app.frag.Request(c.Context(), http.MethodGet, addressURL, nil, address); err != nil {
		return fmt.Errorf("request address failed: %w", err)
	}

	buildingURL := fmt.Sprintf("/buildings/%s", assessment.BuildingID.UUID)
	if err := app.frag.Request(c.Context(), http.MethodGet, buildingURL, nil, building); err != nil {
		return fmt.Errorf("request building failed: %w", err)
	}

	peopleURL := fmt.Sprintf("/people/%s", assessment.PersonID.UUID)
	if err := app.brp.Request(c.Context(), http.MethodGet, peopleURL, nil, person); err != nil {
		return fmt.Errorf("request person failed: %w", err)
	}

	// Create the assessment
	params := &queries.AssessmentCreateParams{
		ID:         uuid.New(),
		AddressID:  assessment.AddressID.UUID,
		BuildingID: assessment.BuildingID.UUID,
		PersonID:   assessment.PersonID.UUID,
		AddressLabel: fmt.Sprintf("%s %s, %s, %s",
			address.Street,
			strings.TrimSpace(fmt.Sprintf("%d %s", address.HouseNumber, address.HouseNumberAddition)),
			address.ZipCode,
			address.Municipality,
		),
		PersonLabel: person.Name,
		WozAmount:   int32(calculateWOZInCents(building)),
		CreatedAt:   time.Now(),
	}

	if err := app.db.Queries.AssessmentCreate(c.Context(), params); err != nil {
		return fmt.Errorf("assessment create failed: %w", err)
	}

	// Add a success message to the session, and redirect the visitor using a HX-Redirect response header
	if err := storeInSession(c, "successMessage", "De OZB-aanslag is succesvol aangemaakt."); err != nil {
		return err
	}

	c.Set("HX-Redirect", "/ozb")

	return nil
}

//nolint:gomnd // unnecessary
func (app *Application) OZBGet(c *fiber.Ctx) error {
	assessmentID, err := uuid.Parse(c.Params("id"))
	if err != nil {
		return fmt.Errorf("uuid parse failed: %w", err)
	}

	// Fetch the OZB assessment
	record, err := app.db.Queries.AssessmentGet(c.Context(), assessmentID)
	if err != nil {
		return fmt.Errorf("assessment get failed: %w", err)
	}

	assessment := adapter.ToWOZ(record)

	ozbTaxAmount := math.Round(float64(assessment.WozAmount)*0.079) / 100
	totalAmount := math.Round((ozbTaxAmount+sewageCharges+wasteCharges)*100) / 100
	monthlyAmount := math.Round(totalAmount*10) / 100
	previousYear := record.CreatedAt.AddDate(-1, 0, 0)

	return c.Render("ozb-details", fiber.Map{
		"assessment":         assessment,
		"base64ID":           base64.RawURLEncoding.EncodeToString(assessment.ID[:]), // base64 encoded for shorter representation
		"createdAtMinusYear": &previousYear,
		// Replace the first comma in the string by a newline and delete the second one. IMPROVE: neater solution
		"fullAddressLabel": strings.Replace(strings.Replace(record.AddressLabel, ",", "\n", 1), ",", "", 1),
		"ozbTaxAmount":     ozbTaxAmount,
		"totalAmount":      totalAmount,
		"monthlyAmount":    monthlyAmount,
	})
}

func (app *Application) OZBDownload(c *fiber.Ctx) error {
	assessmentID, err := uuid.Parse(c.Params("id"))
	if err != nil {
		return fmt.Errorf("uuid parse failed: %w", err)
	}

	// Fetch the OZB assessment
	record, err := app.db.Queries.AssessmentGet(c.Context(), assessmentID)
	if err != nil {
		return fmt.Errorf("assessment get failed: %w", err)
	}

	assessment := adapter.ToWOZ(record)

	pdf := ozbPDFBuild(assessment)

	if pdf.Error() != nil {
		return fmt.Errorf("pdf build failed: %w", err)
	}

	// Note: We cannot use a byte stream to download the PDF directly into the ResponseWriter
	// Because this is not support by fiber
	file, err := os.CreateTemp("/tmp/", "temp-*.pdf")
	if err != nil {
		return fmt.Errorf("could not create temp file: %w", err)
	}

	defer os.Remove(file.Name())

	if err := pdf.Output(file); err != nil {
		return fmt.Errorf("could not write pdf into writer: %w", err)
	}

	name := fmt.Sprintf("ozb-aanslag-%d-%s.pdf", assessment.CreatedAt.Year(), time.Now().Format(time.RFC3339))
	if err = c.Download(file.Name(), name); err != nil {
		return fmt.Errorf("ozb pdf download failed: %w", err)
	}

	return nil
}

//nolint:gomnd // fixed values
func drawLine(pdf *fpdf.Fpdf, x1, y1, x2, y2 float64) {
	pdf.MoveTo(x1, y1)
	pdf.LineTo(x2, y2)
	pdf.ClosePath()
	pdf.SetDrawColor(0xe5, 0xe7, 0xeb)
	pdf.SetFillColor(0xe5, 0xe7, 0xeb)
	pdf.SetLineWidth(.2)
	pdf.DrawPath("DF")
}

//nolint:unparam,varnamelen // can be helpful in the future
func textColumn(pdf *fpdf.Fpdf, x, y, w, h float64, text ...string) {
	for idx := range text {
		pdf.MoveTo(x, y+(float64(idx)*h))

		h := h
		if strings.Contains(text[idx], "\n") {
			h /= 2
		}

		pdf.MultiCell(w, h, text[idx], "", "", false)
	}
}

//nolint:gomnd,funlen // unnecessary
func ozbPDFBuild(assessment *model.OZB) *fpdf.Fpdf {
	ozbTaxAmount := math.Round(float64(assessment.WozAmount)*taxTariff) / 100

	// Note: 237 and 366.25 are fixed values, also in the backend.
	totalAmount := math.Round((ozbTaxAmount+sewageCharges+wasteCharges)*100) / 100
	monthlyAmount := math.Round(totalAmount*10) / 100
	previousYear := assessment.CreatedAt.AddDate(-1, 0, 0)

	font := "DejaVu Sans"

	pdf := fpdf.New("p", "mm", "A4", "./fonts/")
	pdf.AddFont("DejaVu Sans", "", "DejaVuSans.json")
	pdf.AddFont("DejaVu Sans", "B", "DejaVuSans-Bold.json")
	pdf.AddPage()
	pdf.SetFont(font, "B", 12)
	pdf.SetTitle("Aanslagbiljet", true)
	pdf.MoveTo(5, 10)
	pdf.Cell(40, 10, "AANSLAGBILJET")

	pdf.SetFont(font, "B", 10)
	pdf.MoveTo(100, 25)
	pdf.MultiCell(40, 5, fmt.Sprintf("%s\n%s\n%s",
		"Kenmerk",
		"Belastingjaar",
		"Dagtekening",
	), "", "", false)

	pdf.SetFont(font, "", 10)
	pdf.MoveTo(140, 25)
	pdf.MultiCell(60, 5, fmt.Sprintf("%s\n%d\n%s",
		base64.RawURLEncoding.EncodeToString(assessment.ID[:]),
		assessment.CreatedAt.Year(),
		assessment.CreatedAt.Format("02 Jan 2006"),
	), "", "", false)

	pdf.MoveTo(5, 50)
	pdf.MultiCell(60, 5, fmt.Sprintf("%s\n%s",
		assessment.PersonLabel,
		strings.Replace(strings.Replace(assessment.AddressLabel, ", ", "\n", 1), ",", "", 1),
	), "", "L", false)

	pdf.SetFont(font, "B", 8)

	col1, col2, col3, col4, col5, col6 := 35.0, 25., 55., 50., 20., 30.

	heigth := 70.0
	pdf.MoveTo(5, heigth)
	pdf.Cell(col1, 5, "Soort belasting")
	pdf.Cell(col2, 5, "Periode")
	pdf.Cell(col3, 5, "Omschrijving belastingobject")
	pdf.Cell(col4, 5, "Heffingsmaatstaf")
	pdf.Cell(col5, 5, "Tarief")
	pdf.Cell(col6, 5, "Bedrag")

	heigth += 5
	for i := 0; i < 4; i++ {
		drawLine(pdf, 5, heigth+float64(15*i), 205, heigth+float64(15*i))
	}

	col := 5.

	pdf.SetFont(font, "", 8)
	textColumn(pdf, col, heigth, col1, 15,
		"WOZ-beschikking",
		"OZB-eigenaar",
		"Rioolheffing eigenaar",
		"Afvalstoffen heffing")

	col += col1
	textColumn(pdf, col, heigth, col2, 15,
		fmt.Sprintf("jan - dec %d", assessment.CreatedAt.Year()),
		fmt.Sprintf("jan - dec %d", assessment.CreatedAt.Year()),
		fmt.Sprintf("jan - dec %d", assessment.CreatedAt.Year()),
		fmt.Sprintf("jan - dec %d", assessment.CreatedAt.Year()),
	)

	col += col2
	textColumn(pdf, col, heigth, col3, 15,
		strings.Replace(strings.Replace(assessment.AddressLabel, ", ", "\n", 1), ",", "", 1),
		strings.Replace(strings.Replace(assessment.AddressLabel, ", ", "\n", 1), ",", "", 1),
		strings.Replace(strings.Replace(assessment.AddressLabel, ", ", "\n", 1), ",", "", 1),
		strings.Replace(strings.Replace(assessment.AddressLabel, ", ", "\n", 1), ",", "", 1),
	)

	col += col3
	textColumn(pdf, col, heigth, col4, 15,
		fmt.Sprintf("WOZ-waarde: € %d\nWaardepeildatum: 01-01-%d", assessment.WozAmount, previousYear.Year()),
		fmt.Sprintf("WOZ-waarde: € %d", assessment.WozAmount),
		"Aantal aanslagen: 1",
		"Meerpersoonshuishuiden",
	)

	col += col4
	textColumn(pdf, col, heigth, col5, 15,
		"",
		fmt.Sprintf("%.6f %%", taxTariff),
		fmt.Sprintf("€ %.2f", sewageCharges),
		fmt.Sprintf("€ %.2f", wasteCharges),
	)

	col += col5
	textColumn(pdf, col, heigth, col6, 15,
		"",
		fmt.Sprintf("€ %.2f", ozbTaxAmount),
		fmt.Sprintf("€ %.2f", sewageCharges),
		fmt.Sprintf("€ %.2f", wasteCharges),
	)

	heigth += 55

	pdf.MoveTo(176, heigth)
	pdf.SetFont(font, "B", 8)
	pdf.Cell(14, 10, "Totaal: ")
	pdf.SetFont(font, "", 8)
	pdf.Cell(20, 10, fmt.Sprintf("€ %.2f", totalAmount))

	heigth += 7
	pdf.MoveTo(5, heigth)
	pdf.MultiCell(190, 3, fmt.Sprintf(`
	Het aanslagbedraq wordt in 10 gelijke bedragen maandelijks afgeschreven rond de 25e van elke maand. 
	Het termijnbedrag is € %.2f en wordt afgeschreven van rekeningnummer NL02ABNA0123456789. 
	De 1e incassotermijn valt in de eerste maand na dagtekening van de aanslag.`, monthlyAmount), "", "", false)

	return pdf
}

func calculateWOZInCents(building *model.Building) uint64 {
	const (
		M2Price = 3500
	)

	return M2Price * uint64(building.Surface)
}

func (app *Application) OZBDelete(c *fiber.Ctx) error {
	assessmentID, err := uuid.Parse(c.Params("id"))
	if err != nil {
		return fmt.Errorf("uuid parse failed: %w", err)
	}

	// Delete the OZB assessment
	if err = app.db.Queries.AssessmentDelete(c.Context(), assessmentID); err != nil {
		return fmt.Errorf("assessment delete failed: %w", err)
	}

	// Add a success message to the session, and redirect the visitor using a HX-Redirect response header
	if err := storeInSession(c, "successMessage", "De OZB-aanslag is succesvol verwijderd."); err != nil {
		return err
	}

	c.Set("HX-Redirect", "/ozb")

	return nil
}
