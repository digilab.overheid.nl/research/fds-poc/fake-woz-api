package application

import (
	"fmt"
	"net/http"
	"net/url"
	"strings"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fake-woz-api/application/model"
)

func (app *Application) AddressesList(c *fiber.Ctx) error {
	// Fetch the addresses from the backend
	query := url.Values{}
	query.Set("perPage", c.Query("perPage", "10"))
	query.Set("lastId", c.Query("lastId"))
	query.Set("firstId", c.Query("firstId"))

	response := new(model.Response[model.Address])
	if err := app.frag.Request(c.Context(), http.MethodGet, fmt.Sprintf("/addresses?%s", query.Encode()), nil, &response); err != nil {
		return fmt.Errorf("addresses request failed: %w", err)
	}

	// Get a possible success message from the session
	successMessage, err := getAndDeleteFromSession(c, "successMessage")
	if err != nil {
		return err
	}

	return c.Render("address-index", fiber.Map{
		"successMessage": successMessage,
		"addresses":      response.Data,
		"pagination":     response.Pagination,
	})
}

func (app *Application) AddressesNew(c *fiber.Ctx) error {
	return c.Render("address-new", fiber.Map{
		"municipalities": []struct {
			ID    string `json:"id"`
			Label string `json:"label"`
		}{
			{"Riemer", "Riemer"},
			{"Stijns", "Stijns"},
			{"Wielewaal", "Wielewaal"},
			{"Zonnedaal", "Zonnedaal"},
		},
	})
}

func (app *Application) AddressesCreate(c *fiber.Ctx) error {
	address := new(model.Address)
	if err := c.BodyParser(address); err != nil {
		return fmt.Errorf("unmarshal failed: %w", err)
	}

	// Validate the input
	var errs []string // Used to store validation errors

	if address.Street == "" {
		errs = append(errs, "De straat is vereist.")
	}

	if address.HouseNumber == 0 {
		errs = append(errs, "Het huisnummer is vereist.")
	}

	if address.ZipCode == "" {
		errs = append(errs, "De postcode is vereist.")
	}

	if address.Municipality == "" {
		errs = append(errs, "De gemeente is vereist.")
	}

	// Note: Purpose and Surface are not validated

	// In case there are validation errors, return them
	if len(errs) != 0 {
		return c.Render("partials/form-errors", errs, "")
	}

	// Else: send the request to the backend
	address.ID = uuid.New() // Note: the backend does not generate an ID
	if err := app.frag.Request(c.Context(), http.MethodPost, "/addresses", address, nil); err != nil {
		return fmt.Errorf("storing the address failed: %w", err)
	}

	// Add a success message to the session, and redirect the visitor using a HX-Redirect response header
	if err := storeInSession(c, "successMessage", "Het adres is succesvol opgeslagen."); err != nil {
		return err
	}

	c.Set("HX-Redirect", "/addresses")

	return nil
}

func (app *Application) AddressesGet(c *fiber.Ctx) error {
	addressID, err := uuid.Parse(c.Params("id"))
	if err != nil {
		return fmt.Errorf("uuid parse failed: %w", err)
	}

	address := new(model.Address)
	if err := app.frag.Request(c.Context(), http.MethodGet, fmt.Sprintf("/addresses/%s", addressID), nil, address); err != nil {
		return fmt.Errorf("address request failed: %w", err)
	}

	return c.Render("address-details", fiber.Map{
		"address": address,
	})
}

func (app *Application) AddressesDelete(c *fiber.Ctx) error {
	addressID, err := uuid.Parse(c.Params("id"))
	if err != nil {
		return fmt.Errorf("uuid parse failed: %w", err)
	}

	if err := app.frag.Request(c.Context(), http.MethodDelete, fmt.Sprintf("/addresses/%s", addressID), nil, nil); err != nil {
		return fmt.Errorf("address request failed: %w", err)
	}

	// Add a success message to the session, and redirect the visitor using a HX-Redirect response header
	if err := storeInSession(c, "successMessage", "Het adres is succesvol verwijderd."); err != nil {
		return err
	}

	c.Set("HX-Redirect", "/addresses")

	return nil
}

func (app *Application) addressesList(c *fiber.Ctx) ([]simpleAddress, error) {
	// Fetch the addresses
	query := url.Values{}
	query.Set("perPage", c.Query("perPage", "10"))
	query.Set("s", c.Query("s"))

	response := new(model.Response[model.Address])
	if err := app.frag.Request(c.Context(), http.MethodGet, fmt.Sprintf("/addresses?%s", query.Encode()), nil, &response); err != nil {
		return nil, fmt.Errorf("frag request failed: %w", err)
	}

	addresses := response.Data

	simpleAddresses := make([]simpleAddress, 0, len(addresses))
	for idx := range addresses {
		simpleAddresses = append(simpleAddresses, simpleAddress{
			ID: addresses[idx].ID,
			Label: fmt.Sprintf("%s %s, %s, %s",
				addresses[idx].Street,
				strings.TrimSpace(fmt.Sprintf("%d %s", addresses[idx].HouseNumber, addresses[idx].HouseNumberAddition)),
				addresses[idx].ZipCode,
				addresses[idx].Municipality,
			),
		})
	}

	return simpleAddresses, nil
}
