//nolint:wrapcheck // unnecessary
package application

import (
	"html/template"
	"net/http"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/template/html/v2"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fake-woz-api/application/storage"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fake-woz-api/helpers"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fake-woz-api/rc"
)

type Application struct {
	addr   string
	server *fiber.App
	frag   rc.RestCall
	brp    rc.RestCall
	db     *storage.Database
}

func New(listenAddress string, frag, brp rc.RestCall, db *storage.Database) Application {
	return Application{
		addr: listenAddress,
		frag: frag,
		brp:  brp,
		db:   db,
	}
}

func (app *Application) Router() {
	engine := html.New("./views", ".html")

	// Add functions
	engine.AddFuncMap(template.FuncMap{
		"marshal":      helpers.Marshal,
		"formatYear":   helpers.FormatYear,
		"formatDate":   helpers.FormatDate,
		"formatTime":   helpers.FormatTime,
		"numberFormat": helpers.NumberFormat,
		"dict":         helpers.Dict,
		"addInt":       helpers.Add[int], // IMPROVE: define in a generic way? See e.g. https://github.com/gohugoio/hugo/blob/master/tpl/math/math.go
		"subInt":       helpers.Sub[int],
	})

	app.server = fiber.New(fiber.Config{
		Views:       engine,
		ViewsLayout: "layouts/main",
	})

	// Middleware
	app.server.Use(logger.New(logger.Config{
		// Do not log IP addresses. Note: see https://docs.gofiber.io/api/middleware/logger#constants for more logger variables
		Format: "${time} | ${status} | ${latency} | ${method} | ${path}   ${error}\n",
	}))

	app.server.Static("/", "./public")

	// security.txt redirect
	app.server.Get("/.well-known/security.txt", func(c *fiber.Ctx) error {
		return c.Redirect("https://www.ncsc.nl/.well-known/security.txt", fiber.StatusFound) // StatusFound is HTTP code 302
	})

	app.server.Get("/", func(c *fiber.Ctx) error {
		return c.Redirect("/addresses", http.StatusFound) // Note: using StatusFound to avoid browser caching, since the same path (when running locally) is used for other projects
	})

	addresses := app.server.Group("/addresses")
	addresses.Get("/", app.AddressesList)
	addresses.Get("/new", app.AddressesNew)
	addresses.Post("/", app.AddressesCreate)
	addresses.Get("/:id", app.AddressesGet)
	addresses.Delete("/:id", app.AddressesDelete)

	buildings := app.server.Group("/buildings")
	buildings.Get("/", app.BuildingsList)
	buildings.Get("/new", app.BuildingsNew)
	buildings.Post("/", app.BuildingsCreate)
	buildings.Get("/:id", app.BuildingsGet)
	buildings.Delete("/:id", app.BuildingsDelete)

	people := app.server.Group("/people")
	people.Get("/", app.PeopleList)
	people.Get("/new", app.PeopleNew)
	people.Post("/", app.PeopleCreate)
	people.Get("/:id", app.PeopleGet)
	people.Delete("/:id", app.PeopleDelete)

	ozb := app.server.Group("/ozb")
	ozb.Get("/", app.OZBIndex)
	ozb.Get("/new", app.OZBNew)
	ozb.Get("/new/details", app.OZBNewDetails)
	ozb.Post("/", app.OZBCreate)
	ozb.Get("/:id", app.OZBGet)
	ozb.Delete("/:id", app.OZBDelete)
	ozb.Get("/:id/download", app.OZBDownload)
}

func (app *Application) Listen() error {
	return app.server.Listen(app.addr)
}
