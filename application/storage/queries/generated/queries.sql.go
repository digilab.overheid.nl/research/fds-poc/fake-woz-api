// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.22.0
// source: queries.sql

package generated

import (
	"context"
	"time"

	"github.com/google/uuid"
)

const assessmentCreate = `-- name: AssessmentCreate :exec
INSERT INTO digilab_demo_woz.assessments
(id, address_id, building_id, person_id, address_label, person_label, woz_amount, created_at)
VALUES (
    $1, $2, $3, $4, $5, $6, $7, $8 
)
`

type AssessmentCreateParams struct {
	ID           uuid.UUID
	AddressID    uuid.UUID
	BuildingID   uuid.UUID
	PersonID     uuid.UUID
	AddressLabel string
	PersonLabel  string
	WozAmount    int32
	CreatedAt    time.Time
}

func (q *Queries) AssessmentCreate(ctx context.Context, arg *AssessmentCreateParams) error {
	_, err := q.exec(ctx, q.assessmentCreateStmt, assessmentCreate,
		arg.ID,
		arg.AddressID,
		arg.BuildingID,
		arg.PersonID,
		arg.AddressLabel,
		arg.PersonLabel,
		arg.WozAmount,
		arg.CreatedAt,
	)
	return err
}

const assessmentDelete = `-- name: AssessmentDelete :exec
DELETE FROM digilab_demo_woz.assessments
WHERE id=$1
`

func (q *Queries) AssessmentDelete(ctx context.Context, id uuid.UUID) error {
	_, err := q.exec(ctx, q.assessmentDeleteStmt, assessmentDelete, id)
	return err
}

const assessmentGet = `-- name: AssessmentGet :one
SELECT id, address_id, building_id, person_id, address_label, person_label, woz_amount, created_at
FROM digilab_demo_woz.assessments
WHERE id=$1
`

func (q *Queries) AssessmentGet(ctx context.Context, id uuid.UUID) (*DigilabDemoWozAssessment, error) {
	row := q.queryRow(ctx, q.assessmentGetStmt, assessmentGet, id)
	var i DigilabDemoWozAssessment
	err := row.Scan(
		&i.ID,
		&i.AddressID,
		&i.BuildingID,
		&i.PersonID,
		&i.AddressLabel,
		&i.PersonLabel,
		&i.WozAmount,
		&i.CreatedAt,
	)
	return &i, err
}

const assessmentList = `-- name: AssessmentList :many
SELECT id, address_id, building_id, person_id, address_label, person_label, woz_amount, created_at
FROM digilab_demo_woz.assessments
`

func (q *Queries) AssessmentList(ctx context.Context) ([]*DigilabDemoWozAssessment, error) {
	rows, err := q.query(ctx, q.assessmentListStmt, assessmentList)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	items := []*DigilabDemoWozAssessment{}
	for rows.Next() {
		var i DigilabDemoWozAssessment
		if err := rows.Scan(
			&i.ID,
			&i.AddressID,
			&i.BuildingID,
			&i.PersonID,
			&i.AddressLabel,
			&i.PersonLabel,
			&i.WozAmount,
			&i.CreatedAt,
		); err != nil {
			return nil, err
		}
		items = append(items, &i)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}
