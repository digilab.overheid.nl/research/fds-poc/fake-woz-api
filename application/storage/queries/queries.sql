-- name: AssessmentList :many
SELECT id, address_id, building_id, person_id, address_label, person_label, woz_amount, created_at
FROM digilab_demo_woz.assessments;

-- name: AssessmentGet :one
SELECT id, address_id, building_id, person_id, address_label, person_label, woz_amount, created_at
FROM digilab_demo_woz.assessments
WHERE id=$1;

-- name: AssessmentCreate :exec
INSERT INTO digilab_demo_woz.assessments
(id, address_id, building_id, person_id, address_label, person_label, woz_amount, created_at)
VALUES (
    $1, $2, $3, $4, $5, $6, $7, $8 
);

-- name: AssessmentDelete :exec
DELETE FROM digilab_demo_woz.assessments
WHERE id=$1;
