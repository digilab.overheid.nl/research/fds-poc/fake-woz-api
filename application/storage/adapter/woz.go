package adapter

import (
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fake-woz-api/application/model"
	queries "gitlab.com/digilab.overheid.nl/research/fds-poc/fake-woz-api/application/storage/queries/generated"
)

func ToWOZ(record *queries.DigilabDemoWozAssessment) *model.OZB {
	return &model.OZB{
		ID:           record.ID,
		AddressID:    record.AddressID,
		BuildingID:   record.BuildingID,
		PersonID:     record.PersonID,
		AddressLabel: record.AddressLabel,
		PersonLabel:  record.PersonLabel,
		WozAmount:    record.WozAmount,
		CreatedAt:    record.CreatedAt,
	}
}
