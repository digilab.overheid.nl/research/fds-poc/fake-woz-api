BEGIN transaction;

CREATE SCHEMA digilab_demo_woz;

CREATE TABLE digilab_demo_woz.assessments (
    id UUID NOT NULL,
    address_id UUID NOT NULL,
    building_id UUID NOT NULL,
    person_id UUID NOT NULL,

    address_label TEXT NOT NULL,
    person_label TEXT NOT NULL,

    woz_amount INT NOT NULL,

    created_at TIMESTAMP NOT NULL,

    PRIMARY KEY(id)
);

CREATE INDEX idx_assessment_address_id ON digilab_demo_woz.assessments(address_id);
CREATE INDEX idx_assessment_building_id ON digilab_demo_woz.assessments(building_id);
CREATE INDEX idx_assessment_person_id ON digilab_demo_woz.assessments(person_id);

COMMIT;
