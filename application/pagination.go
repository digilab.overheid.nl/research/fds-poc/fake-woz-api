package application

import (
	"strconv"
)

type pagination struct {
	Pages      []int // Note: 0 means: ellipsis (...)
	PageNumber int
	NumPages   int
}

const (
	rowsPerPage = 10
	onEachSide  = 1
	onEnds      = 1
)

// rng returns a range of integers from `from` to `to`, inclusive. See also the note below about the Python range() function
func rng(from, to int) []int {
	pages := make([]int, 0, to-from+1) // Note: we assume that to >= from. The code below could be optimized a bit, but is written like this for clarity reasons
	for i := from; i <= to; i++ {
		pages = append(pages, i)
	}

	return pages
}

// getPagination returns 'elided' pagination for the specified number of rows and page query string parameter, similar to what e.g. Django has, see https://docs.djangoproject.com/en/dev/ref/paginator/#django.core.paginator.Paginator.get_elided_page_range
func getPagination(numberOfRows int, pageParameter string) (res pagination, lowerBound int, upperBound int) {
	numPages := max((numberOfRows+rowsPerPage-1)/rowsPerPage, 1) // Note: similar to math.Ceil
	pageNumber := 1                                              // Default page

	if num, _ := strconv.Atoi(pageParameter); num > 1 {
		pageNumber = min(num, numPages)
	}

	// Set the values to be used as lower and upper bounds for slicing the results
	lowerBound = max((pageNumber-1)*rowsPerPage, 0)
	upperBound = min(pageNumber*rowsPerPage, numberOfRows) // Note: may equal to the slice length, since a half-open range is used for slicing, see https://go.dev/blog/slices-intro

	res.PageNumber = pageNumber
	res.NumPages = numPages

	// Note: the logic below is similar to that used in the Django code, see https://github.com/django/django/blob/main/django/core/paginator.py, except that the rng function is including the second argument, unlike the python range() function

	// Short range: return it
	if numPages <= (onEachSide+onEnds)*2 {
		res.Pages = rng(1, numPages)

		return
	}

	if pageNumber > onEachSide+onEnds+2 {
		res.Pages = append(append(
			rng(1, onEnds),
			0), // Ellipsis
			rng(pageNumber-onEachSide, pageNumber)...)
	} else {
		res.Pages = rng(1, pageNumber)
	}

	if pageNumber < numPages-onEachSide-onEnds-1 {
		res.Pages = append(append(
			append(res.Pages, rng(pageNumber+1, pageNumber+onEachSide)...),
			0), // Ellipsis
			rng(numPages-onEnds+1, numPages)...)
	} else {
		res.Pages = append(res.Pages, rng(pageNumber+1, numPages)...)
	}

	return
}
