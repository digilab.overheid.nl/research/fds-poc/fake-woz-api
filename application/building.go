package application

import (
	"fmt"
	"net/http"
	"net/url"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fake-woz-api/application/model"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fake-woz-api/helpers"
)

func (app *Application) BuildingsList(c *fiber.Ctx) error {
	// Fetch the buildings from the backend
	query := url.Values{}
	query.Set("perPage", c.Query("perPage", "10"))
	query.Set("lastId", c.Query("lastId"))
	query.Set("firstId", c.Query("firstId"))

	response := new(model.Response[model.Building])
	if err := app.frag.Request(c.Context(), http.MethodGet, fmt.Sprintf("/buildings?%s", query.Encode()), nil, &response); err != nil {
		return fmt.Errorf("buildings request failed: %w", err)
	}

	// Get a possible success message from the session
	successMessage, err := getAndDeleteFromSession(c, "successMessage")
	if err != nil {
		return err
	}

	return c.Render("building-index", fiber.Map{
		"successMessage": successMessage,
		"buildings":      response.Data,
		"pagination":     response.Pagination,
	})
}

func (app *Application) BuildingsNew(c *fiber.Ctx) error {
	addresses, err := app.addressesList(c)
	if err != nil {
		return err
	}

	if string(c.Request().Header.Peek("Hx-Request")) == "true" {
		return c.Render("partials/building-new-address-list", fiber.Map{
			"addresses": addresses,
		}, "")
	}

	return c.Render("building-new", fiber.Map{
		"addresses": addresses,
	})
}

func (app *Application) BuildingsCreate(c *fiber.Ctx) error {
	// Use a custom struct to override / extend some data
	building := new(struct {
		model.Building
		ConstructedAt helpers.FormattedTime `json:"constructedAt"` // Override the ConstructedAt property
		Address       helpers.NullUUID      `json:"address"`       // Note: NullUUID does not work with empty strings to null (string) values. See also the comments in helpers.go
	})
	if err := c.BodyParser(building); err != nil {
		return fmt.Errorf("unmarshal failed: %w", err)
	}

	// Validate the input
	var errs []string // Used to store validation errors

	if building.ConstructedAt.IsZero() {
		errs = append(errs, "De bouwdatum is vereist.")
	}

	if building.Surface == 0 {
		errs = append(errs, "De oppervlakte is vereist.")
	}

	if !building.Address.Valid {
		errs = append(errs, "Het adres is vereist.")
	}

	// In case there are validation errors, return them
	if len(errs) != 0 {
		return c.Render("partials/form-errors", errs, "")
	}

	// Else: send the request to the backend
	building.ID = uuid.New() // Note: the backend does not generate an ID
	building.Building.ConstructedAt = time.Time(building.ConstructedAt)
	if err := app.frag.Request(c.Context(), http.MethodPost, "/buildings", building.Building, nil); err != nil {
		return fmt.Errorf("storing the address failed: %w", err)
	}

	// Attach the address to the building
	if err := app.frag.Request(c.Context(), http.MethodPost, "/buildings/attach/address", model.BuildingAttach{
		Buildings: []uuid.UUID{building.ID},
		Addresses: []uuid.UUID{building.Address.UUID},
	}, nil); err != nil {
		return fmt.Errorf("attaching the address to the building failed: %w", err)
	}

	// Add a success message to the session, and redirect the visitor using a HX-Redirect response header
	if err := storeInSession(c, "successMessage", "Het gebouw is succesvol opgeslagen."); err != nil {
		return err
	}

	c.Set("HX-Redirect", "/buildings")

	return nil
}

func (app *Application) BuildingsGet(c *fiber.Ctx) error {
	buildingID, err := uuid.Parse(c.Params("id"))
	if err != nil {
		return fmt.Errorf("uuid parse failed: %w", err)
	}

	building := new(model.Building)
	if err := app.frag.Request(c.Context(), http.MethodGet, fmt.Sprintf("/buildings/%s", buildingID), nil, building); err != nil {
		return fmt.Errorf("building request failed: %w", err)
	}

	return c.Render("building-details", fiber.Map{
		"building": building,
	})
}

func (app *Application) BuildingsDelete(c *fiber.Ctx) error {
	buildingID, err := uuid.Parse(c.Params("id"))
	if err != nil {
		return fmt.Errorf("uuid parse failed: %w", err)
	}

	if err := app.frag.Request(c.Context(), http.MethodDelete, fmt.Sprintf("/buildings/%s", buildingID), nil, nil); err != nil {
		return fmt.Errorf("building request failed: %w", err)
	}

	// Add a success message to the session, and redirect the visitor using a HX-Redirect response header
	if err := storeInSession(c, "successMessage", "Het gebouw is succesvol verwijderd."); err != nil {
		return err
	}

	c.Set("HX-Redirect", "/buildings")

	return nil
}
