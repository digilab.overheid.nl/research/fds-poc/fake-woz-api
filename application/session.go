package application

import (
	"fmt"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/session"
)

// sessionStore contains a Fiber session store. Note: currently in-memory, since we assume single deployment
var sessionStore = session.New()

// storeInSession stores the specified key-value combination in the session store
func storeInSession(c *fiber.Ctx, key string, value interface{}) error {
	// Get the session from storage
	sess, err := sessionStore.Get(c)
	if err != nil {
		return fmt.Errorf("error getting session: %w", err)
	}

	// Set the message in the store a save the session
	sess.Set(key, value)

	if err := sess.Save(); err != nil {
		return fmt.Errorf("error saving session: %w", err)
	}

	return nil
}

// getAndDeleteFromSession deletes and returns the value corresponding to the specified key from the session. Defaults to nil if not found
func getAndDeleteFromSession(c *fiber.Ctx, key string) (interface{}, error) {
	// Get the session from storage
	sess, err := sessionStore.Get(c)
	if err != nil {
		return nil, fmt.Errorf("error getting session: %w", err)
	}

	// Get the message from the store, delete it, and save the session
	message := sess.Get(key)
	sess.Delete(key)
	sess.Save()

	return message, nil
}
