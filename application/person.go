package application

import (
	"fmt"
	"net/http"
	"net/url"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fake-woz-api/application/model"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fake-woz-api/helpers"
)

func (app *Application) PeopleList(c *fiber.Ctx) error {
	// Fetch the buildings from the backend
	query := url.Values{}
	query.Set("perPage", c.Query("perPage", "10"))
	query.Set("lastId", c.Query("lastId"))
	query.Set("firstId", c.Query("firstId"))

	response := new(model.Response[model.Person])
	if err := app.brp.Request(c.Context(), http.MethodGet, fmt.Sprintf("/people?%s", query.Encode()), nil, &response); err != nil {
		return fmt.Errorf("people request failed: %w", err)
	}

	// Get a possible success message from the session
	successMessage, err := getAndDeleteFromSession(c, "successMessage")
	if err != nil {
		return err
	}

	return c.Render("person-index", fiber.Map{
		"successMessage": successMessage,
		"people":         response.Data,
		"pagination":     response.Pagination,
	})
}

func (app *Application) PeopleNew(c *fiber.Ctx) error {
	addresses, err := app.addressesList(c)
	if err != nil {
		return err
	}

	if string(c.Request().Header.Peek("Hx-Request")) == "true" {
		return c.Render("partials/person-new-address-list", fiber.Map{
			"addresses": addresses,
		}, "")
	}

	return c.Render("person-new", fiber.Map{
		"addresses": addresses,
	})
}

func (app *Application) PeopleCreate(c *fiber.Ctx) error {
	// Use a custom struct to override / extend some data
	person := new(struct {
		model.Person
		BornAt              helpers.FormattedTime `json:"bornAt"`              // Override the BornAt property
		DiedAt              helpers.FormattedTime `json:"diedAt"`              // Override the DiedAt property
		RegisteredAddressID helpers.NullUUID      `json:"registeredAddressId"` // Note: NullUUID does not work with empty strings to null (string) values. See also the comments in helpers.go
	})
	if err := c.BodyParser(person); err != nil {
		return fmt.Errorf("unmarshal failed: %w", err)
	}

	// Validate the input
	var errs []string // Used to store validation errors

	if person.Name == "" {
		errs = append(errs, "De naam is vereist.")
	}

	if person.Sex == "" {
		errs = append(errs, "Het geslacht is vereist.")
	}

	if person.BSN == "" {
		errs = append(errs, "Het burgerservicenummer is vereist.")
	}

	if person.BornAt.IsZero() {
		errs = append(errs, "De geboortedatum is vereist.")
	}

	if !person.RegisteredAddressID.Valid {
		errs = append(errs, "Het adres is vereist.")
	}

	// Note: DiedAt is not validated

	// In case there are validation errors, return them
	if len(errs) != 0 {
		return c.Render("partials/form-errors", errs, "")
	}

	// Else: send the request to the backend
	person.ID = uuid.New() // Note: the backend does not generate an ID
	person.Person.RegisteredAddressID = &person.RegisteredAddressID.UUID

	if !person.BornAt.IsZero() {
		t := time.Time(person.BornAt)
		person.Person.BornAt = &t
	}

	if !person.DiedAt.IsZero() {
		t := time.Time(person.DiedAt)
		person.Person.DiedAt = &t
	}

	if err := app.brp.Request(c.Context(), http.MethodPost, "/people", person.Person, nil); err != nil {
		return fmt.Errorf("storing the person failed: %w", err)
	}

	// Add a success message to the session, and redirect the visitor using a HX-Redirect response header
	if err := storeInSession(c, "successMessage", "De persoon is succesvol opgeslagen."); err != nil {
		return err
	}

	c.Set("HX-Redirect", "/people")

	return nil
}

func (app *Application) PeopleGet(c *fiber.Ctx) error {
	personID, err := uuid.Parse(c.Params("id"))
	if err != nil {
		return fmt.Errorf("uuid parse failed: %w", err)
	}

	person := new(model.Person)
	if err := app.brp.Request(c.Context(), http.MethodGet, fmt.Sprintf("/people/%s", personID), nil, person); err != nil {
		return fmt.Errorf("person request failed: %w", err)
	}

	// If an address reference exists, fetch the corresponding address
	address := new(model.Address)
	if person.RegisteredAddressID != nil {
		if err := app.frag.Request(c.Context(), http.MethodGet, fmt.Sprintf("/addresses/%s", *person.RegisteredAddressID), nil, address); err != nil {
			return fmt.Errorf("address request failed: %w", err)
		}
	}

	return c.Render("person-details", fiber.Map{
		"person":  person,
		"address": address,
	})
}

func (app *Application) PeopleDelete(c *fiber.Ctx) error {
	personID, err := uuid.Parse(c.Params("id"))
	if err != nil {
		return fmt.Errorf("uuid parse failed: %w", err)
	}

	if err := app.brp.Request(c.Context(), http.MethodDelete, fmt.Sprintf("/people/%s", personID), nil, nil); err != nil {
		return fmt.Errorf("person request failed: %w", err)
	}

	// Add a success message to the session, and redirect the visitor using a HX-Redirect response header
	if err := storeInSession(c, "successMessage", "De persoon is succesvol verwijderd."); err != nil {
		return err
	}

	c.Set("HX-Redirect", "/people")

	return nil
}
