module gitlab.com/digilab.overheid.nl/research/fds-poc/fake-woz-api

go 1.21.1

require (
	github.com/gofiber/fiber/v2 v2.49.2
	github.com/gofiber/template/html/v2 v2.0.5
	github.com/golang-migrate/migrate/v4 v4.16.2
	github.com/google/uuid v1.3.1
	github.com/huandu/xstrings v1.4.0
	github.com/jmoiron/sqlx v1.3.5
	github.com/lib/pq v1.10.2
	github.com/spf13/cobra v1.7.0
	golang.org/x/exp v0.0.0-20230315142452-642cacee5cc0
	golang.org/x/text v0.13.0
)

require (
	github.com/andybalholm/brotli v1.0.5 // indirect
	github.com/go-pdf/fpdf v0.9.0 // indirect
	github.com/gofiber/template v1.8.2 // indirect
	github.com/gofiber/utils v1.1.0 // indirect
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/hashicorp/go-multierror v1.1.1 // indirect
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/klauspost/compress v1.17.0 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	github.com/mattn/go-runewidth v0.0.15 // indirect
	github.com/rivo/uniseg v0.4.4 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasthttp v1.50.0 // indirect
	github.com/valyala/tcplisten v1.0.0 // indirect
	go.uber.org/atomic v1.7.0 // indirect
	golang.org/x/net v0.15.0 // indirect
	golang.org/x/sys v0.12.0 // indirect
	golang.org/x/tools v0.13.0 // indirect
)
