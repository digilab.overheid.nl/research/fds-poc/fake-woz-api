package cmd

import (
	"log"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fake-woz-api/application"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fake-woz-api/application/storage"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fake-woz-api/rc"
)

var serveOpts struct { //nolint:gochecknoglobals // this is the recommended way to use cobra
	ListenAddress  string
	PostgresDSN    string
	FRAGBackendURL string
	FRPBackendURL  string
}

func init() { //nolint:gochecknoinits,gocyclo // this is the recommended way to use cobra
	serveCommand.Flags().StringVarP(&serveOpts.ListenAddress, "listen-address", "", "0.0.0.0:8081", "Address for the api to listen on.")
	serveCommand.Flags().StringVarP(&serveOpts.PostgresDSN, "postgres-dsn", "", "", "Postgres Connection URL")
	serveCommand.Flags().StringVarP(&serveOpts.FRAGBackendURL, "frag-backend-url", "", "http://0.0.0.0:9000", "backend url for the frag")
	serveCommand.Flags().StringVarP(&serveOpts.FRPBackendURL, "frp-backend-url", "", "http://0.0.0.0:9010", "backend url for the frag")

	// Required flags
	if err := serveCommand.MarkFlagRequired("postgres-dsn"); err != nil {
		log.Fatal(err)
	}
}

var serveCommand = &cobra.Command{ //nolint:gochecknoglobals // this is the recommended way to use cobra
	Use:   "serve",
	Short: "Start the api",
	Run: func(cmd *cobra.Command, args []string) {
		err := storage.PostgresPerformMigrations(serveOpts.PostgresDSN)
		if err != nil {
			log.Fatalf("failed to migrate db: %v", err)
		}

		db, err := storage.New(serveOpts.PostgresDSN)
		if err != nil {
			log.Fatalf("failed to connect to the database: %v", err)
		}

		frag := rc.New("frag", serveOpts.FRAGBackendURL)
		frp := rc.New("frp", serveOpts.FRPBackendURL)

		app := application.New(serveOpts.ListenAddress, frag, frp, db)

		app.Router()

		if err := app.Listen(); err != nil {
			log.Fatal(err)
		}

		os.Exit(0)
	},
}
