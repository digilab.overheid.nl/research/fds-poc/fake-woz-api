---
title : "FDS PoC - Fictieve WOZ Dashboard"
description: "Frontend to visualize transactions that happen in the FDS PoC."
date: 2023-09-28T09:14:40+02:00
draft: false
toc: true
---

## Running locally

Clone this repo. Note: static files (css/scss/js/img) are stored in the `static` dir, but built into the `public` dir.

When running for the first time, make sure `pnmp` is installed (e.g. using `brew install pnpm`) and run `pnpm install`.

Run:

```sh
pnpm run dev
go run main.go -port 8080
```

This starts a web server locally on the specified port.

# Running locally in docker
To run the web server in docker on port 8081 

Run: 
```shell
make build

make up
```

To stop the developement server
```shell
make down 
```

To view the logs of the server
```shell
make logs
```