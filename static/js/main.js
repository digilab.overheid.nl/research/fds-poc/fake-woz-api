// Imports
import './htmx.js'; // Note: imported this way, since some plugins need the global `htmx` variable, which requires the use of an extra file according to https://htmx.org/docs/#webpack (which indeed seems to be the case)

import { TempusDominus } from '@eonasdan/tempus-dominus';

// Common web components that are used on several pages. Note: intentionally written in vanilla JS, since this is preferred over a framework with possible vendor lock-in

// ESDropdown is a dropdown with filter
class ESDropdown extends HTMLElement {
  constructor() {
    // Always call super first in the constructor
    super();

    // Set properties
    this.isOpen = false;
    this.value = this.getAttribute('value');

    // Create a wrapper div with position: relative
    const wrap = document.createElement('div');
    wrap.className = 'block relative';

    // Create a button element
    this.button = document.createElement('button');
    this.button.setAttribute('type', 'button');
    this.button.setAttribute('id', this.getAttribute('id'));
    this.removeAttribute('id'); // Remove the `id` attribute from the Web Component, so the browser knows which element to focus when a label for it is clicked
    this.button.className = 'border border-gray-300 text-gray-800 bg-white text-sm rounded-lg focus:outline-none focus:ring-1 focus:ring-sky-500 focus:border-sky-500 p-2.5 flex items-center w-full shadow-sm';
    this.button.innerHTML = `<span class="grow text-left text-gray-400">${this.getAttribute('placeholder')}</span> <svg class="w-4 h-4 ml-2" aria-hidden="true" fill="none" stroke="currentColor"
      viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
      <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7"></path>
    </svg>`;

    // Create a hidden input, so the element can be used in regular forms. Note: using a hidden input, since modifying formData of this.closest('form') on submit seems to be difficult to combine with HTMX. Support for the ElementInternals interface (see https://webkit.org/blog/13711/elementinternals-and-form-associated-custom-elements/) is currently still limited
    this.hiddenInput = document.createElement('input');
    this.hiddenInput.setAttribute('type', 'hidden');
    this.hiddenInput.setAttribute('name', this.getAttribute('name'));
    this.hiddenInput.setAttribute('value', this.value);

    // Create the menu element
    this.menu = document.createElement('div');
    this.menu.className = 'hidden absolute left-0 z-10 bg-white rounded shadow border border-gray-300 divide-y divide-gray-200';

    // Create the filter input element
    this.filter = document.createElement('input');
    this.filter.setAttribute('type', 'text');
    this.filter.setAttribute('placeholder', 'Filter…');
    this.filter.className = 'text-gray-800 bg-transparent text-sm block w-full px-4 py-3 border-0 focus:outline-none placeholder:text-gray-400 focus:ring-0';

    // Create an element that contains the dropdown items
    this.list = document.createElement('ul');
    this.list.className = 'py-2 text-sm text-gray-700 max-h-60 overflow-y-auto';
    this.list.setAttribute('id', 'list');

    // Create the list items
    const items = JSON.parse(this.getAttribute('choices'));
    const val = this.getAttribute('value');

    if (items != null) {
      items.forEach(item => {
        const el = document.createElement('li');
        el.className = 'block px-4 py-2 hover:bg-gray-100 cursor-pointer';
        el.innerText = item.label;
        this.list.appendChild(el); 

        // In case a value is selected, set it in the button span
        if (item.id === val) {
          span.innerText = item.label;
          span.className = 'grow text-left'; // Remove the gray text color
        }

        // Add a click event to the item element
        el.addEventListener('click', () => {
          this.value = item.id;
          span.innerText = item.label;
          span.className = 'grow text-left'; // Remove the gray text color
          hidden.setAttribute('value', this.value);

          this.close();

          // Fire a 'change' event
          this.dispatchEvent(new Event('change'));
        });
      });
    }

    // Append and insert the elements
    this.menu.replaceChildren(this.filter, this.list);
    wrap.replaceChildren(this.button, this.hiddenInput, this.menu);
    this.appendChild(wrap);

    // Add events
    this.button.addEventListener('click', (e) => {
      if (this.isOpen) {
        this.close();
      } else {
        this.open();
      }

      e.stopPropagation();
    })

    // Close the dropdown when clicked outside
    this.handleClick = ((e) => {
      if (!(this.menu.contains(e.target))) {
        this.close();
      }
    }).bind(this);
    document.addEventListener('click', this.handleClick);

    // Close the dropdown when the Esc key is pressed
    this.handleKeyDown = ((e) => {
      if (e.key === 'Escape' || e.key === 'Esc') {
        this.close();
      }
    }).bind(this);
    document.addEventListener('keydown', this.handleKeyDown);

    // Filter events
    this.filter.addEventListener('input', () => {
      const query = this.filter.value.trim().toLowerCase();

      Array.from(this.list.children).forEach(el => {
        if (el.innerText.toLowerCase().includes(query)) {
          el.classList.remove('hidden');
        } else {
          el.classList.add('hidden');
        }
      });

      // In case of 0 results, show an element that tells this
      // TODO
    });

    // Support up and down arrows, prevent default behaviour
    // TODO
  }

  open() {
    this.menu.classList.remove('hidden');
    this.isOpen = true;

    this.filter.focus();
    this.filter.select();

    // IMPROVE: set z-index of the menu higher than currently open dropdown menus and datepickers, or close other dropdowns
  }

  close() {
    this.menu.classList.add('hidden');
    this.isOpen = false;
  }

  // // connectedCallback runs each time the element is appended to or moved in the DOM
  // connectedCallback() {
  //   console.log('connected!', this);
  // }

  // disconnectedCallback runs when the element is removed from the DOM
  disconnectedCallback() {
    // Remove the click and keydown events from the document
    document.removeEventListener('click', this.handleClick);
    document.removeEventListener('keydown', this.handleKeyDown);
  }
}

class ESDropdownWithSearch extends ESDropdown {
  constructor() {
    super();

    const el = document.getElementById("template");
    this.list.innerHTML = el.innerHTML;

    this.filter.setAttribute("hx-trigger", this.getAttribute("hx-trigger"));
    this.filter.setAttribute("hx-get", this.getAttribute("hx-get"));
    this.filter.setAttribute("hx-vals", this.getAttribute("hx-vals"));
    this.filter.setAttribute("hx-target", this.getAttribute("hx-target"));

    this.removeAttribute("hx-trigger");
    this.removeAttribute("hx-get");
    this.removeAttribute("hx-vals");
    this.removeAttribute("hx-target");

    document.body.addEventListener('htmx:load', ((evt) => {
      Array.from(this.list.children).forEach(el => {
        el.addEventListener('click', (() => {
          const span = this.button.querySelector('span');
          span.innerText = el.innerText;
          span.className = 'grow text-left';

          this.hiddenInput.setAttribute('value', el.getAttribute("id"));

          this.close();

          this.dispatchEvent(new Event('change'));
        }).bind(this));
      })
    }).bind(this))
  }
}

const datepickerOptions = {
  display: {
    theme: 'light',
    icons: {
      type: 'sprites',
      time: '/img/feather-sprite.svg#clock',
      date: '/img/feather-sprite.svg#calendar',
      up: '/img/feather-sprite.svg#arrow-up',
      down: '/img/feather-sprite.svg#arrow-down',
      previous: '/img/feather-sprite.svg#arrow-left',
      next: '/img/feather-sprite.svg#arrow-right',
    },
    buttons: {
      today: false,
      clear: false,
      close: false,
    }
  },
  localization: {
    // today: 'Go to today',
    // clear: 'Clear selection',
    // close: 'Close the picker',
    // selectMonth: 'Select Month',
    // previousMonth: 'Previous Month',
    // nextMonth: 'Next Month',
    // selectYear: 'Select Year',
    // previousYear: 'Previous Year',
    // nextYear: 'Next Year',
    // selectDecade: 'Select Decade',
    // previousDecade: 'Previous Decade',
    // nextDecade: 'Next Decade',
    // previousCentury: 'Previous Century',
    // nextCentury: 'Next Century',
    // pickHour: 'Pick Hour',
    // incrementHour: 'Increment Hour',
    // decrementHour: 'Decrement Hour',
    // pickMinute: 'Pick Minute',
    // incrementMinute: 'Increment Minute',
    // decrementMinute: 'Decrement Minute',
    // pickSecond: 'Pick Second',
    // incrementSecond: 'Increment Second',
    // decrementSecond: 'Decrement Second',
    // toggleMeridiem: 'Toggle Meridiem',
    // selectTime: 'Select Time',
    // selectDate: 'Select Date',
    dayViewHeaderFormat: { month: 'long', year: 'numeric' },
    locale: 'default',
    startOfTheWeek: 1,
    hourCycle: 'h23',
    dateFormats: {
      LTS: 'HH:mm:ss',
      LT: 'HH:mm',
      L: 'dd-MM-yyyy',
      LL: 'd MMMM, yyyy',
      LLL: 'd MMMM, yyyy HH:mm',
      LLLL: 'dddd, d MMMM, yyyy HH:mm'
    },
  }
};

// ESDateTimePicker consists of a text input with datetimepicker
class ESDateTimePicker extends HTMLElement {
  constructor() {
    // Always call super first in the constructor
    super();

    // Set properties
    this.value = this.getAttribute('value') || '';

    // Create a wrapper div
    const wrap = document.createElement('div');
    wrap.className = 'relative';

    // Create an icon div element
    const icon = document.createElement('div');
    icon.className = 'absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none';
    icon.innerHTML = `<svg aria-hidden="true" class="w-5 h-5 text-gray-500" fill="currentColor" viewBox="0 0 20 20"
        xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd"
          d="M6 2a1 1 0 00-1 1v1H4a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V6a2 2 0 00-2-2h-1V3a1 1 0 10-2 0v1H7V3a1 1 0 00-1-1zm0 5a1 1 0 000 2h8a1 1 0 100-2H6z"
          clip-rule="evenodd"></path>
      </svg>`;

    // Create an input element
    const input = document.createElement('input');
    input.setAttribute('id', this.getAttribute('id'));
    this.removeAttribute('id'); // Remove the `id` attribute from the Web Component, so the browser knows which element to focus when a label for it is clicked
    input.setAttribute('name', this.getAttribute('name'));
    input.setAttribute('type', 'text');
    input.setAttribute('value', this.value);
    input.setAttribute('placeholder', this.getAttribute('placeholder'));
    input.setAttribute('autocomplete', 'off');
    input.className = 'bg-white border border-gray-300 text-gray-800 text-sm rounded-lg focus:outline-none focus:ring-1 focus:ring-sky-500 focus:border-sky-500 block w-full p-2.5 pl-10 shadow-sm placeholder:text-gray-400 ' + this.getAttribute('extra-input-classes');

    // Append and insert the elements
    wrap.replaceChildren(icon, input);
    this.replaceChildren(wrap);

    // Initialize the datetimepicker
    new TempusDominus(input, datepickerOptions);

    // Do not let the change event on the input propagate to the Web Component
    input.addEventListener('change', (e) => {
      e.stopPropagation();
    })

    // On hide, fire the change event on the Web Component
    input.addEventListener('hide.td', () => {
      this.value = input.value;
      this.dispatchEvent(new Event('change'));
    })
  }
}


// Define the web components
if ('customElements' in window) {
  customElements.define('es-dropdown', ESDropdown);
  customElements.define('es-dropdown-with-search', ESDropdownWithSearch);
  customElements.define('es-datetimepicker', ESDateTimePicker);
}
