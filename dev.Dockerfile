FROM golang:1.21-alpine

WORKDIR /api

RUN ["go", "install", "github.com/githubnemo/CompileDaemon@latest"]

ENTRYPOINT CompileDaemon -log-prefix=false -pattern="(.+\.go|.+\.c|.+\.sql|.+\.toml|.+\.html)$" -exclude-dir=.git -build="go build -o api ." -command="./api serve"
