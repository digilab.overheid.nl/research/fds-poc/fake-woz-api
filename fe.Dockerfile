FROM node:18.17.1 as base

ENV PNPM_HOME="/pnpm"
ENV PATH="$PNPM_HOME:$PATH"
RUN corepack enable

WORKDIR /app

COPY package.json .
COPY pnpm-lock.yaml .
COPY tailwind.config.js .
COPY .postcssrc .

RUN pnpm install

ENTRYPOINT pnpm run dev