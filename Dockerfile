# Stage 1
FROM alpine:3.18 as sqlc

COPY --from=sqlc/sqlc:1.22.0 /workspace/sqlc /usr/bin/sqlc

WORKDIR /gen

COPY ./application/storage .

WORKDIR /gen/queries 

RUN sqlc generate


# Stage 2
FROM golang:1.21-alpine3.18 AS builder

# Copy the code into the container. Note: copy to a dir instead of `.`, since $GOPATH may not contain a go.mod file
WORKDIR /build

COPY . .
COPY --from=sqlc /gen/queries/generated /build/pkg/storage/queries/generated

# Build the Go files
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-s -w" -o server .


# Stage 3
FROM node:18-alpine3.18 AS node_builder

# Copy the code into the container. Note: copy to a dir instead of `.`, since Parcel cannot run in the root dir, see https://github.com/parcel-bundler/parcel/issues/6578
WORKDIR /build
COPY . .

# Build the static files
RUN corepack enable && corepack prepare pnpm@latest --activate
RUN pnpm install
RUN pnpm run build


# Stage 4
FROM alpine:3.18

# Add timezones
RUN apk add --no-cache tzdata

# Copy the binary from /build to the root folder of this container. Also copy the public dir that was built
COPY --from=builder /build/server /
COPY --from=node_builder /build/public /public

# Copy the view dir, which is read during runtime
COPY views views

# Run the binary when starting the container
ENTRYPOINT ["/server"]

EXPOSE 80
