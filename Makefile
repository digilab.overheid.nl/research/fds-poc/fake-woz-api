dir = $(shell pwd)

default:
	-docker network create digilab-demo-woz

build:
	docker-compose build

up: default sqlc
	docker-compose up -d --remove-orphans

logs:
	docker-compose logs -f

down:
	docker-compose down --remove-orphans

sqlc:
	docker run --rm -v $(dir)/application/storage:/src -w /src/queries sqlc/sqlc:1.22.0 generate

psql:
	docker-compose exec postgres psql -U postgres