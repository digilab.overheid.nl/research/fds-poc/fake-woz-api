package helpers

import (
	"bytes"
	"encoding/json"
	"errors"
	"log"
	"strconv"
	"strings"
	"time"

	"github.com/google/uuid"
	"golang.org/x/exp/constraints"
	"golang.org/x/text/language"
	"golang.org/x/text/message"
	"golang.org/x/text/number"
)

var printer = message.NewPrinter(language.Dutch)

var monthsReplacer = strings.NewReplacer( // Use a strings replacer to format the time in Dutch language. See time.shortMonthNames. IMPROVE: neater solution using a library
	"Jan", "januari",
	"Feb", "februari",
	"Mar", "maart",
	"Apr", "april",
	"May", "mei",
	"Jun", "juni",
	"Jul", "juli",
	"Aug", "augustus",
	"Sep", "september",
	"Oct", "oktober",
	"Nov", "november",
	"Dec", "december",
)

// amsLocation refers to the Dutch time zone / location
var amsLocation *time.Location

// FormattedTime is used to unmarshal datetime in Dutch format directly from POST bodies
type FormattedTime time.Time

// NullUUID is an alternative for uuid.NullUUID, since Fiber's BodyParser uses UnmarshalText for multipart form data and uuid.NullUUID only detects null values in its UnmarshalJSON method. Note: possibly unnecessary once 'omitzero' is implemented, see https://github.com/golang/go/discussions/63397
type NullUUID uuid.NullUUID

func init() {
	var err error
	if amsLocation, err = time.LoadLocation("Europe/Amsterdam"); err != nil {
		log.Fatalf("error getting time zone: %v", err)
	}
}

func (nu *NullUUID) UnmarshalText(data []byte) error {
	if nu == nil {
		return errors.New("cannot unmarshal to nil pointer")
	}

	if len(data) == 0 || bytes.Equal(data, []byte("null")) {
		nu.Valid = false
		return nil
	}

	nui := uuid.NullUUID(*nu)
	if err := nui.UnmarshalText(data); err != nil {
		return err
	}

	nu.UUID = nui.UUID
	nu.Valid = nui.Valid

	return nil
}

func (ft FormattedTime) MarshalText() ([]byte, error) {
	return []byte(time.Time(ft).In(amsLocation).Format("02-01-2006 15:04")), nil
}

func (ft *FormattedTime) UnmarshalText(b []byte) error {
	if ft == nil {
		return errors.New("cannot unmarshal to nil pointer")
	}

	val := strings.TrimSpace(strings.Trim(string(b), `"`)) // Get rid the encapsulating quotes (note: quotes are not used in form data, but in case of JSON, which may be used later) and possible whitespace
	if val == "" || val == "null" {
		return nil
	}

	// Parse the time
	t, err := time.ParseInLocation("02-01-2006 15:04", val, amsLocation)
	if err != nil {
		return err
	}

	// Set the result
	*ft = FormattedTime(t)
	return nil
}

func (ft FormattedTime) IsZero() bool {
	return time.Time(ft).IsZero()
}

// FormatTime formats the specified time in the Dutch time zone and language
func FormatTime(t *time.Time) string {
	if t == nil || t.IsZero() {
		return "(Onbekend)"
	}

	return monthsReplacer.Replace(t.In(amsLocation).Format("2 Jan 2006 15:04"))
}

// FormatDate formats the specified time in the Dutch time zone and language
func FormatDate(t *time.Time) string {
	if t == nil || t.IsZero() {
		return "(Onbekend)"
	}

	return monthsReplacer.Replace(t.In(amsLocation).Format("2 Jan 2006"))
}

// FormatYear returns the year of the specief time.Time instance in the Dutch time zone
func FormatYear(t *time.Time) string {
	if t == nil || t.IsZero() {
		return "(Onbekend)"
	}

	return strconv.Itoa(t.In(amsLocation).Year())
}

// Marshal marshals data to JSON format (escaped). Note: return as template.JS to get unescaped code, see https://pkg.go.dev/html/template#JS
func Marshal(v interface{}) string {
	val, err := json.Marshal(v)
	if err != nil {
		// Only log the error, do not return
		log.Println(err)
	}

	return string(val)
}

// NumberFormat formats the specified number as string. IMPROVE: access an interface argument (or generic) and support multiple number types
func NumberFormat(v interface{}) string {
	if v == nil {
		return ""
	}

	switch v.(type) {
	case int, int8, int16, int32, int64, uint, uint16, uint32, uint64:
		// Integer: show the number without decimals
		return printer.Sprint(v)
	default:
		// Otherwise (e.g. float): assume the number is a currency, so show the number with two fixed digits
		return printer.Sprint(number.Decimal(v, number.MinFractionDigits(2), number.MaxFractionDigits(2)))
	}

	// IMPROVE: use a library like github.com/shopspring/decimal for decimal numbers instead of float64, see https://github.com/shopspring/decimal#why-dont-you-just-use-float64
}

// Dict creates a new map from the given parameters by treating values as key-value pairs. The number of values must be even. See https://github.com/Masterminds/sprig/blob/master/dict.go and https://github.com/gohugoio/hugo/blob/master/tpl/collections/collections.go
func Dict(v ...interface{}) (map[string]interface{}, error) {
	if len(v)%2 != 0 {
		return nil, errors.New("invalid dict call, the number of arguments must be even")
	}

	dict := make(map[string]interface{}, len(v)/2)
	for i := 0; i < len(v); i += 2 {
		key, ok := v[i].(string)
		if !ok {
			return nil, errors.New("invalid dict key, must be string")
		}

		dict[key] = v[i+1]
	}

	return dict, nil
}

type Number interface {
	constraints.Integer | constraints.Float
}

func Add[T Number](a, b T) T {
	return a + b
}

func Sub[T Number](a, b T) T {
	return a - b
}
